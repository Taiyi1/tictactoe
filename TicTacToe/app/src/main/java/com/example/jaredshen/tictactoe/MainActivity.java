package com.example.jaredshen.tictactoe;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;


public class MainActivity extends AppCompatActivity {
    public final static String MODE = "Player";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    //**Creates game vs player**//
    public void vsPlayer(View view) {
        Intent intent = new Intent(this, BoardActivity.class);
        intent.putExtra(MODE, "Player");
        startActivity(intent);
    }

    //**Creates game vs easy AI**//
    public void vsEasyComp(View view) {
        Intent intent = new Intent(this, BoardActivity.class);
        intent.putExtra(MODE, "Easy");
        startActivity(intent);
    }

    //**Creates game vs medium AI**//
    public void vsMediumComp(View view) {
        Intent intent = new Intent(this, BoardActivity.class);
        intent.putExtra(MODE, "Medium");
        startActivity(intent);
    }

    //** Creates game vs hard AI**//
    public void vsHardComp(View view) {
        Intent intent = new Intent(this, BoardActivity.class);
        intent.putExtra(MODE, "Hard");
        startActivity(intent);
    }

    //**Opens High Scores**//
    public void HighScores(View view) {
        Intent intent = new Intent(this, ScoreActivity.class);
        startActivity(intent);
    }

    //**Opens Credits**//
    public void Credits(View view) {
        Intent intent = new Intent(this, CreditsActivity.class);
        startActivity(intent);
    }

}
