package com.example.jaredshen.tictactoe;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class ScoreActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String easyScore = preferences.getString("Easy", "0");
        String mediumScore = preferences.getString("Medium", "0");
        String hardScore = preferences.getString("Hard", "0");

        easyScore = "Easy Mode: " + easyScore;
        mediumScore = "Medium Mode: " + mediumScore;
        hardScore = "Hard Mode: " + hardScore;

        TextView score;
        score = (TextView) findViewById(R.id.Easy);
        score.setText(easyScore);
        score = (TextView) findViewById(R.id.Medium);
        score.setText(mediumScore);
        score = (TextView) findViewById(R.id.Hard);
        score.setText(hardScore);
    }

    //**Returns back to main menu**//
    public void back(View view)
    {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
