package com.example.jaredshen.tictactoe;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class CreditsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credits);
    }

        //**Returns back to main menu**//
        public void back(View view)
        {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
}
