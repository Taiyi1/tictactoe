package com.example.jaredshen.tictactoe;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Jared Shen on 11/27/2016.
 */

public class TicTacToeBoard {
    private String[][] board = new String[3][3];

    //**Constructs a board**//
    public TicTacToeBoard() {
        for (int x = 0; x < 3; x++) {
            for (int y = 0; y < 3; y++) {
                board[x][y] = " ";
            }
        }
    }

    //**Clears a board of all X's and O's**//
    public void clearBoard() {
        for (int x = 0; x < 3; x++) {
            for (int y = 0; y < 3; y++) {
                board[x][y] = " ";
            }
        }
    }

    //**Returns symbol at point**//
    public String getPoint(int x, int y) {
        return board[x][y];
    }

    //**Places symbol at point**//
    public void setPoint(int x, int y, String symbol) {
        board[x][y] = symbol;
    }

    //**Returns true if player(X or O) has a winning board.**//
    public boolean isWinner(String player) {
        if (board[0][0] == player && board[1][1] == player && board[2][2] == player) {
            return true;
        }
        if (board[0][2] == player && board[1][1] == player && board[2][0] == player) {
            return true;
        }
        for (int x = 0; x < 3; x++) {
            if (board[x][0] == player && board[x][1] == player && board[x][2] == player) {
                return true;
            }
        }
        for (int y = 0; y < 3; y++) {
            if (board[0][y] == player && board[1][y] == player && board[2][y] == player) {
                return true;
            }
        }
        return false;
    }

    //**Returns true if there are no more possible moves**//
    public boolean isFull() {
        for (int x = 0; x < 3; x++) {
            for (int y = 0; y < 3; y++) {
                if (board[x][y] == " ") {
                    return false;
                }
            }
        }
        return true;
    }

    //**Returns AI's move.  Difficulty determines how the algorithm**//
    public int[] compMove(String difficulty, String symbol) {
        Random random = new Random();
        String enemySymbol;
        if (symbol == "X") {
            enemySymbol = "O";
        } else {
            enemySymbol = "X";
        }
        print();

        if (difficulty.equals("Medium") || difficulty.equals("Hard")) {
            System.out.println("Searching");
            for (int x = 0; x < 3; x++) {
                for (int y = 0; y < 3; y++) {
                    if (board[x][y] == " ") {
                        String[][] newBoard = new String[3][3];
                        copy(board, newBoard);
                        newBoard[x][y] = symbol;
                        if (isWinner(symbol, newBoard)) {
                            return new int[]{x, y};
                        }
                        newBoard[x][y] = enemySymbol;
                        if (isWinner(enemySymbol, newBoard)) {
                            return new int[]{x, y};
                        }
                    }
                }
            }
        }
        if (difficulty.equals("Hard"))
        {
            System.out.println("Hard Mode");
            if (board[0][0] == " ")
            {
                return new int[]{0,0};
            }
            if (board[0][2] == " ")
            {
                return new int[]{0,2};
            }
            if (board[2][0] == " ")
            {
                return new int[]{2,0};
            }
            if (board[2][2] == " ")
            {
                return new int[]{2,2};
            }
            if (board[1][1] == " ")
            {
                return new int[]{1,1};
            }
            if (board[0][1] == " ")
            {
                return new int[]{0,1};
            }
            if (board[1][0] == " ")
            {
                return new int[]{1,0};
            }
            if (board[1][2] == " ")
            {
                return new int[]{1,2};
            }
            if (board[2][1] == " ")
            {
                return new int[]{2,1};
            }
        }
        while (true)
        {
            int x = random.nextInt(3);
            int y = random.nextInt(3);
            if (board[x][y] == " ")
            {
                return new int[] {x,y};
            }
        }
    }


    //**Returns true if player(X or O) has a winning board. For AI searching.**//
    private boolean isWinner(String player, String[][] tempBoard) {
        if (tempBoard[0][0] == player && tempBoard[1][1] == player && tempBoard[2][2] == player) {
            return true;
        }
        if (tempBoard[0][2] == player && tempBoard[1][1] == player && tempBoard[2][0] == player) {
            return true;
        }
        for (int x = 0; x < 3; x++) {
            if (tempBoard[x][0] == player && tempBoard[x][1] == player && tempBoard[x][2] == player) {
                return true;
            }
        }
        for (int y = 0; y < 3; y++) {
            if (tempBoard[0][y] == player && tempBoard[1][y] == player && tempBoard[2][y] == player) {
                return true;
            }
        }
        return false;
    }

    //**Copies the values of firstBoard into secondBoard**//
    public void copy(String[][] firstBoard, String[][] secondBoard)
    {
        for (int x = 0; x < 3; x++) {
            for (int y = 0; y < 3; y++) {
                secondBoard[x][y] = firstBoard[x][y];
            }
        }
    }

    //**Prints out current board to console. For testing purposes.**//
    public void print()
    {
        System.out.println(board[0][0] + " | " + board[0][1] + " | " + board[0][2]);
        System.out.println(board[1][0] + " | " + board[1][1] + " | " + board[1][2]);
        System.out.println(board[2][0] + " | " + board[2][1] + " | " + board[2][2]);
    }

}
