package com.example.jaredshen.tictactoe;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Arrays;
import java.util.Objects;

public class BoardActivity extends AppCompatActivity {
    public TicTacToeBoard board;
    public String difficulty;
    public String current = "X";
    public int winStreak = 0;
    public boolean lock = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tic_tac_toe_board);

        board = new TicTacToeBoard();
        Intent intent = getIntent();
        difficulty = intent.getStringExtra(MainActivity.MODE);
    }

    /**Switches clicked button to player's mark then runs AI if needed**/
    public void click(View view) {
        if (lock == true)
        {
            return;
        }
        TextView button = (TextView) findViewById(view.getId());
        int x = 0;
        int y = 0;
        switch (button.getId())
        {
            case R.id.r0c0:
                x = 0;
                y = 0;
                break;
            case R.id.r0c1:
                x = 0;
                y = 1;
                break;
            case R.id.r0c2:
                x = 0;
                y = 2;
                break;
            case R.id.r1c0:
                x = 1;
                y = 0;
                break;
            case R.id.r1c1:
                x = 1;
                y = 1;
                break;
            case R.id.r1c2:
                x = 1;
                y = 2;
                break;
            case R.id.r2c0:
                x = 2;
                y = 0;
                break;
            case R.id.r2c1:
                x = 2;
                y = 1;
                break;
            case R.id.r2c2:
                x = 2;
                y = 2;
                break;
        }
        if (board.getPoint(x, y) == " ")
        {
            board.setPoint(x, y, current);
            button.setText(current);

            endTurn();


            if (!difficulty.equals("Player") && lock == false && !board.isFull())
            {
                int[] compMove = board.compMove(difficulty, current);
                x = compMove[0];
                y = compMove[1];

                if (x == 0 && y == 0)
                {
                    button = (TextView) findViewById(R.id.r0c0);
                }
                else if (x == 0 && y == 1)
                {
                    button = (TextView) findViewById(R.id.r0c1);
                }
                else if (x == 0 && y == 2)
                {
                    button = (TextView) findViewById(R.id.r0c2);
                }
                else if (x == 1 && y == 0)
                {
                    button = (TextView) findViewById(R.id.r1c0);
                }
                else if (x == 1 && y == 1)
                {
                    button = (TextView) findViewById(R.id.r1c1);
                }
                else if (x == 1 && y == 2)
                {
                    button = (TextView) findViewById(R.id.r1c2);
                }
                else if (x == 2 && y == 0)
                {
                    button = (TextView) findViewById(R.id.r2c0);
                }
                else if (x == 2 && y == 1)
                {
                    button = (TextView) findViewById(R.id.r2c1);
                }
                else if (x == 2 && y == 2)
                {
                    button = (TextView) findViewById(R.id.r2c2);
                }
                board.setPoint(x, y, current);
                button.setText(current);
                endTurn();
            }


        }

    }

    //**Checks and handles winning or full boards.  Returns true if game is finished**//
    public void endTurn()
    {
        TextView button = (TextView) findViewById(R.id.Turn);
        if (board.isWinner(current))
        {
            winStreak();
            if (current == "X") {
                button.setText("Player 1 Winner!");
            } else {
                current = "X";
                button.setText("Player 2 Winner!");
            }
            lock = true;
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    // Do something after 5s = 5000ms
                    clearBoard();
                }
            }, 2000);
        }
        else if (board.isFull())
        {
            button.setText("Tie Game!");
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    // Do something after 5s = 5000ms
                    clearBoard();
                }
            }, 2000);
        }
        else
        {
            if (current =="X") {
                current = "O";
                button.setText("Player 2 Turn");
            } else {
                current = "X";
                button.setText("Player 1 Turn");
            }
        }
    }

    //**Clears board and resets turn count**//
    public void clearBoard()
    {

        TextView button;
        button = (TextView) findViewById(R.id.Turn);
        button.setText("Player 1 Turn");

        board.clearBoard();

        button = (TextView) findViewById(R.id.r0c0);
        button.setText(" ");
        button = (TextView) findViewById(R.id.r0c1);
        button.setText(" ");
        button = (TextView) findViewById(R.id.r0c2);
        button.setText(" ");
        button = (TextView) findViewById(R.id.r1c0);
        button.setText(" ");
        button = (TextView) findViewById(R.id.r1c1);
        button.setText(" ");
        button = (TextView) findViewById(R.id.r1c2);
        button.setText(" ");
        button = (TextView) findViewById(R.id.r2c0);
        button.setText(" ");
        button = (TextView) findViewById(R.id.r2c1);
        button.setText(" ");
        button = (TextView) findViewById(R.id.r2c2);
        button.setText(" ");

        current = "X";
        lock = false;
    }

    //**Increments winstreak if vs AI, updating records if needed**//
    public void winStreak()
    {
        if (difficulty.equals("Player") || current == "O")
        {
            winStreak = 0;
            return;
        }
        winStreak++;
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);

        int oldStreak = Integer.parseInt(preferences.getString(difficulty, "0"));

        if (winStreak > oldStreak)
        {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(difficulty, Integer.toString(winStreak));
            editor.apply();
        }
    }

    //**Handles the reset button.**//
    public void reset(View view)
    {
        clearBoard();
        winStreak = 0;
    }

    //**Returns back to main menu**//
    public void back(View view)
    {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
